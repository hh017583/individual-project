import pygame
import random

POSITION_D = 258 + 140
POSITION_F = 329 + 140
POSITION_J = 400 + 140
POSITION_K = 471 + 140

end_y = 640

class NoteD(pygame.sprite.Sprite):

    def __init__(self, screen, id, strumTime):
        pygame.sprite.Sprite.__init__(self)
        self.idnum = id
        self.strum = strumTime
        self.miss = False
        self.hit = False
        self.difference = -20000
        self.image = pygame.image.load('Assets/noteD.png')
        self.rect = self.image.get_rect()
        self.rect.move_ip(POSITION_D, -100)

    def update(self, pressed, time):
        if self.hit or self.miss:
            self.kill()
        if not self.hit and not self.miss and self.rect.centery > end_y:
            self.miss = True
            self.difference = -10000
        if not self.miss and pressed and self.rect.centery > 480:
            if not self.hit:
                pressed = False
                self.difference = self.strum - time
            self.hit = True

    def move(self, shift):
        if shift > 0:
            self.rect.centery = shift


class NoteF(pygame.sprite.Sprite):

    def __init__(self, screen, id, strumTime):
        pygame.sprite.Sprite.__init__(self)
        self.idnum = id
        self.strum = strumTime
        self.miss = False
        self.hit = False
        self.difference = -20000
        self.image = pygame.image.load('Assets/noteF.png')
        self.rect = self.image.get_rect()
        self.rect.move_ip(POSITION_F, -100)

    def update(self, pressed, time):
        if self.hit or self.miss:
            self.kill()
        if not self.hit and not self.miss and self.rect.centery > end_y:
            self.miss = True
            self.difference = -10000
        if not self.miss and pressed and self.rect.centery > 480:
            if not self.hit:
                pressed = False
                self.difference = self.strum - time
            self.hit = True

    def move(self, shift):
        if shift > 0:
            self.rect.centery = shift


class NoteJ(pygame.sprite.Sprite):

    def __init__(self, screen, id, strumTime):
        pygame.sprite.Sprite.__init__(self)
        self.idnum = id
        self.strum = strumTime
        self.miss = False
        self.hit = False
        self.difference = -20000
        self.image = pygame.image.load('Assets/noteJ.png')
        self.rect = self.image.get_rect()
        self.rect.move_ip(POSITION_J, -100)

    def update(self, pressed, time):
        if self.hit or self.miss:
            self.kill()
        if not self.hit and not self.miss and self.rect.centery > end_y:
            self.miss = True
            self.difference = -10000
        if not self.miss and pressed and self.rect.centery > 480:
            if not self.hit:
                pressed = False
                self.difference = self.strum - time
            self.hit = True

    def move(self, shift):
        if shift > 0:
            self.rect.centery = shift


class NoteK(pygame.sprite.Sprite):

    def __init__(self, screen, id, strumTime):
        pygame.sprite.Sprite.__init__(self)
        self.idnum = id
        self.strum = strumTime
        self.miss = False
        self.hit = False
        self.difference = -20000
        self.image = pygame.image.load('Assets/noteK.png')
        self.rect = self.image.get_rect()
        self.rect.move_ip(POSITION_K, -100)

    def update(self, pressed, time):
        if self.hit or self.miss:
            self.kill()
        if not self.hit and not self.miss and self.rect.centery > end_y:
            self.miss = True
            self.difference = -10000
        if not self.miss and pressed and self.rect.centery > 480:
            if not self.hit:
                pressed = False
                self.difference = self.strum - time
            self.hit = True

    def move(self, shift):
        if shift > 0:
            self.rect.centery = shift
