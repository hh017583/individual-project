import os
import pygame

from Game import SongLib
from options import main_settings

# from main import clock, font_name, draw_text

clock = pygame.time.Clock()
font_name = pygame.font.match_font('arial')


def draw_text(surf, text, size, color, x, y):
    f = pygame.font.Font(font_name, size)
    textobj = f.render(text, True, color)
    text_rect = textobj.get_rect()
    text_rect.topleft = (x, y)
    surf.blit(textobj, text_rect)

class StartScreen:

    def __init__(self,width, height, screen):
        pygame.display.set_caption("StartScreen")
        self.width = width
        self.height = height
        self.surf = screen
        self.running = True
        self.mouse_click = False
        self.SongLibInterface = SongLib(width, height, surf=screen)

    def run(self):
         # draw menu
        print('run')

        while self.running:
            self.surf.fill((0, 0, 0))

            mx, my = pygame.mouse.get_pos()

            # b1 = pane(screen, 'Game Start', 'arial', 25, (0,0,0), (182,182,182), 50, 100)
            button_1 = pygame.Rect(50, 100, 200, 50)
            button_2 = pygame.Rect(50, 200, 200, 50)

            if button_1.collidepoint((mx, my)):
                if self.mouse_click:
                    #game(self.surf)
                    self.SongLibInterface.run()
            if button_2.collidepoint((mx, my)):
                if self.mouse_click:
                    main_settings(self.surf)

            pygame.draw.rect(self.surf, (182, 182, 182), button_1)
            draw_text(self.surf, 'Game Start', 25, (255, 0, 0), 200 / 2, 110)

            #pygame.draw.rect(screen, (182, 182, 182), button_2)
            #draw_text(screen, 'Options', 25, (255, 0, 0), 200 / 2, 210)
            # input
            self.mouse_click = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouse_click = True
                # manager.process_events(event)

            # update

            # screen.blit(screen, background, (0, 0))
            draw_text(self.surf, 'Welcome', 64, (255, 255, 255), self.width / 1.5, self.height / 4)
            clock.tick(60)  # flash rate
            pygame.display.update()


