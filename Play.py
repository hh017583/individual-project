import csv
from Song import Song
import sys
import pygame
from pygame.locals import *
from pygame import mixer
import Note
import threading
from DFHJ_indicator import D_indicator, F_indicator, J_indicator, K_indicator

clock = pygame.time.Clock()
font_name = pygame.font.match_font('arial')
width = 1080
height = 720
pause = False
running = True
isfinish = False

SPEED = 3
VISUAL_LATENCY = 80
STRUM_POSITION = 720


class Play:
    def getSongFromList(self, id, song_list):
        for song in song_list:
            print(song)
            if id == song[0]:
                self.song = song[6]
                self.map = song[7]
                # print('debut:: selected ' + str(id))

    def __init__(self, _songid, song_list, surf):
        self.songid = _songid
        self.song = type(None)
        self.map = type(None)
        self.getSongFromList(self.songid, song_list)
        print(self.song)
        # self.song = song_path
        self.SongObj = Song(self.song)
        self.width = width
        self.height = height
        # game function var
        self.combo = 0
        self.score = 0
        self.hits=[]
        self.pause = False
        # self.map = map_path
        self.surf = surf
        mixer.init()
        mixer.music.load(self.song)
        self.NOTE_TIMES = []
        self.NOTE_POSITIONS = []

        self.medx = (width - 5) / 2
        self.medy = (height - 600) / 2

        self.d_ind = D_indicator(self.surf, self.medx)
        self.f_ind = F_indicator(self.surf, self.medx)
        self.j_ind = J_indicator(self.surf, self.medx)
        self.k_ind = K_indicator(self.surf, self.medx)

        self.t_kd = threading.Thread(target=self.d_ind.run)
        self.t_kf = threading.Thread(target=self.f_ind.run)
        self.t_kj = threading.Thread(target=self.j_ind.run)
        self.t_kk = threading.Thread(target=self.k_ind.run)

    def restart(self):
        self.play()

    def combo_count(self, combo):
        font = pygame.font.SysFont("comicsansms", 30)
        text = font.render(str(combo), True, (255, 255, 255))
        self.surf.blit(text, (20, 20))

    def score_count(self, score):
        self.score = score
        font = pygame.font.SysFont("comicsansms", 30)
        text = font.render("Score: " + str(score), True, (255, 255, 255))
        self.surf.blit(text, (820, 20))

    def millis_to_normal(self, songtime):
        st = songtime
        sec = (st * 0.001) % 60
        mins = (st / (1000 * 60)) % 60
        return '{}:{}'.format(int(mins), int(sec))

    def duration_to_millis(self, dur):
        t = dur
        hours = t // 3600
        t %= 3600
        mins = t // 60
        t %= 60
        sec = t

        sum = mins + hours / 60
        sum = sum / 60 + sec
        sum = sum * 1000
        return sum

    def songprocess(self, songtime):
        font = pygame.font.SysFont("comicsansms", 30)
        text = font.render(self.millis_to_normal(songtime) + " / " + self.SongObj.length_inString, True,
                           (255, 255, 255))
        self.surf.blit(text, (640 / 2, 20))

    def draw_income_note_text(self, songtime):
        range = 5000
        arr = []
        font = pygame.font.SysFont("comicsansms", 30)
        for i in self.NOTE_TIMES:
            note_time = i
            dur = self.duration_to_millis(note_time)
            if dur > songtime:
                d = dur - songtime
                if d < range:
                    arr.append(i)
        for j in arr:
            text = font.render(str(j), True, (255, 255, 255))
            self.surf.blit(text, (900, 50 + j * 10))

    def draw_freq(self):
        frequency, format, channels = pygame.mixer.get_init()
        font = pygame.font.SysFont("comicsansms", 30)
        text = font.render("{}:{}:{}".format(frequency, format, channels), True,
                           (255, 255, 255))
        self.surf.blit(text, (120, 300))

    def draw_text(self, text, size, x, y):
        font = pygame.font.Font(font_name, size)
        textobj = font.render(text, True, (255, 255, 255))
        text_rect = textobj.get_rect()
        text_rect.topleft = (x, y)
        self.surf.blit(textobj, text_rect)

    # run into another loop to pause the game

    def paused(self):
        # surf = self.surf
        color = (128, 128, 128)
        print('debug.. paused')
        while self.pause:
            #mx, my = pygame.mouse.get_pos()
            # show menu
            menu_width = self.width / 1.5
            menu_height = self.height / 1.5
            menu = pygame.Rect((self.width / 2 - menu_width / 2),
                               (self.height / 2 - menu_height / 2),
                               menu_width, menu_height)

            pygame.draw.rect(self.surf, color, menu, border_radius=0,
                             border_top_left_radius=-1,
                             border_top_right_radius=-1,
                             border_bottom_left_radius=-1,
                             border_bottom_right_radius=-1)
            sub_btn_width = 300
            sub_btn_height = 70
            # resume
            resume_btn = pygame.Rect((self.width / 2 - sub_btn_width / 2),
                                     100 + (sub_btn_height),
                                     sub_btn_width, sub_btn_height)
            pygame.draw.rect(self.surf, (182, 182, 182), resume_btn)
            # restart
            restart_btn = pygame.Rect((self.width / 2 - sub_btn_width / 2),
                                      200 + (sub_btn_height),
                                      sub_btn_width, sub_btn_height)
            pygame.draw.rect(self.surf, (182, 182, 182), restart_btn)
            # quit
            quit_btn = pygame.Rect((self.width / 2 - sub_btn_width / 2),
                                   300 + (sub_btn_height),
                                   sub_btn_width, sub_btn_height)
            pygame.draw.rect(self.surf, (182, 182, 182), quit_btn)

            # show text

            self.draw_text('Resume', 32, (self.width / 2 - sub_btn_width / 2), 100 + (sub_btn_height))
            self.draw_text('Restart', 32, (self.width / 2 - sub_btn_width / 2), 200 + (sub_btn_height))
            self.draw_text('Quit', 32, (self.width / 2 - sub_btn_width / 2), 300 + (sub_btn_height))

            mx, my = pygame.mouse.get_pos()
            mouse_click = False

            # display all objects
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.pause = False
                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        mouse_click = True
            if resume_btn.collidepoint((mx, my)):
                if mouse_click:
                    self.pause = False
            if restart_btn.collidepoint((mx, my)):
                if mouse_click:
                    self.restart()
            if quit_btn.collidepoint((mx, my)):
                if mouse_click:
                    from Game import SongLib
                    self.t_kd.join()
                    self.t_kf.join()
                    self.t_kj.join()
                    self.t_kk.join()
                    back_to_menu = SongLib(self.width, self.height,surf=self.surf)
                    back_to_menu.run()
                    #game(screen=self.surf)
            pygame.display.update()
            clock.tick(15)
        # pygame.mixer.unpause()

    def initialize(self):
        with open(self.map, newline='') as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            lis = list(csvReader)
            r1 = lis[0]
            r2 = lis[1]
            self.NOTE_TIMES = [int(time) for time in r1]
            self.NOTE_POSITIONS = [pos for pos in r2]
            # print(self.NOTE_TIMES, self.NOTE_POSITIONS)

    def play(self):
        running = True
        first_time = True
        pause = self.pause

        previousframetime = clock.get_time()
        lastplayheadposition = 0
        mostaccurate = 0

        self.initialize()
        current = 0
        combo = 0
        score = 0

        notesD = pygame.sprite.Group()
        notesF = pygame.sprite.Group()
        notesJ = pygame.sprite.Group()
        notesK = pygame.sprite.Group()

        nd = []
        nf = []
        nj = []
        nk = []

        for note in range(len(self.NOTE_TIMES)):
            timing = int(self.NOTE_TIMES[note])
            position = str(self.NOTE_POSITIONS[note])

            if position == "d":
                notesD.add(Note.NoteD(self.surf, note, timing))
                nd.append(timing)
            elif position == "f":
                notesF.add(Note.NoteF(self.surf, note, timing))
                nf.append(timing)
            elif position == "j":
                notesJ.add(Note.NoteJ(self.surf, note, timing))
                nj.append(timing)
            elif position == "k":
                notesK.add(Note.NoteK(self.surf, note, timing))
                nk.append(timing)



        counter, text = 3, '3'.rjust(3)
        pygame.time.set_timer(pygame.USEREVENT, 1000)
        font = pygame.font.SysFont('Consolas', 64)

        # count down start
        while first_time:
            ## user input ##
            for e in pygame.event.get():
                if e.type == pygame.USEREVENT:
                    if counter > 0:
                        text = str(counter).rjust(2)
                        counter -= 1
                    if e.type == pygame.QUIT:
                        pygame.quit()

            if counter <= 0:
                first_time = False

            self.surf.fill((0, 0, 0))
            self.surf.blit(font.render(text, True, (255, 255, 255)), (width / 2 - 64 * 1.5, height / 2 - 64))
            pygame.display.flip()
            clock.tick(60)
        # count down end

        # current = clock.get_time()
        # play the song
        pygame.mixer.music.play(0)

        # indicate key pressed

        self.t_kd.start()
        self.t_kj.start()
        self.t_kf.start()
        self.t_kk.start()

        while running:
            songtime = pygame.mixer.music.get_pos()

            keypressD = False
            keypressF = False
            keypressK = False
            keypressJ = False

            current += clock.get_time()
            mostaccurate += current - previousframetime
            previousframetime = current

            if songtime != lastplayheadposition:
                mostaccurate = (mostaccurate + songtime) / 2
                lastplayheadposition = songtime


            ## user Input ##
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.pause = True
                        self.d_ind.destroy()
                        self.f_ind.destroy()
                        self.j_ind.destroy()
                        self.k_ind.destroy()
                        pygame.mixer.music.pause()
                        self.paused()

                        # unpause
                        pygame.mixer.music.unpause()
                        # d
                        self.d_ind = D_indicator(self.surf, self.medx)
                        self.t_kd = threading.Thread(target=self.d_ind.run)
                        self.t_kd.start()
                        # f
                        self.f_ind = F_indicator(self.surf, self.medx)
                        self.t_kf = threading.Thread(target=self.f_ind.run)
                        self.t_kf.start()
                        # j
                        self.j_ind = J_indicator(self.surf, self.medx)
                        self.t_kj = threading.Thread(target=self.j_ind.run)
                        self.t_kj.start()
                        # k
                        self.k_ind = K_indicator(self.surf, self.medx)
                        self.t_kk = threading.Thread(target=self.k_ind.run)
                        self.t_kk.start()

                    if event.key == pygame.K_d:
                        keypressD = True
                    if event.key == pygame.K_f:
                        keypressF = True

                    if event.key == pygame.K_j:
                        keypressJ = True

                    if event.key == pygame.K_k:
                        keypressK = True


            ## actuall game start
            # self.draw_text('actual game here', 24, width / 2.5, height / 4)
            notesD.update(keypressD, mostaccurate)
            notesF.update(keypressF, mostaccurate)
            notesJ.update(keypressJ, mostaccurate)
            notesK.update(keypressK, mostaccurate)

            for note in notesD:
                distance = STRUM_POSITION - (note.strum / SPEED - mostaccurate / SPEED) + VISUAL_LATENCY
                #'Debug: ' + str(note.idnum) + ". " + str(distance)
                note.move(distance)
                statusc = note.difference
                if -5000 <= statusc <= 5000:
                    combo += 1
                    score += 1000
                    self.hits.append('hit')
                elif statusc == -10000:
                    combo = 0
                    self.hits.append('miss')
                if note.hit == True:
                    self.hits.append('hit')
                    #print('hit')
                if note.miss == True:
                    self.hits.append('miss')
                    #print('miss')
            for note in notesF:
                distance = STRUM_POSITION - (note.strum / SPEED - mostaccurate / SPEED) + VISUAL_LATENCY
                #'Debug: ' + str(note.idnum) + ". " + str(distance)
                note.move(distance)
                statusc = note.difference
                if -5000 <= statusc <= 5000:
                    combo += 1
                    score += 1000
                    self.hits.append('hit')
                elif statusc == -10000:
                    combo = 0
                    self.hits.append('miss')
                if note.hit == True:
                    self.hits.append('hit')
                    #print('hit')
                if note.miss == True:
                    self.hits.append('miss')
                    #print('miss')
            for note in notesJ:
                distance = STRUM_POSITION - (note.strum / SPEED - mostaccurate / SPEED) + VISUAL_LATENCY
                #'Debug: ' + str(note.idnum) + ". " + str(distance)
                note.move(distance)
                statusc = note.difference
                if -5000 <= statusc <= 5000:
                    combo += 1
                    score += 1000
                    self.hits.append('hit')
                elif statusc == -10000:
                    combo = 0
                    self.hits.append('miss')
                if note.hit == True:
                    self.hits.append('hit')
                    #print('hit')
                if note.miss == True:
                    self.hits.append('miss')
                    #print('miss')
            for note in notesK:
                distance = STRUM_POSITION - (note.strum / SPEED - mostaccurate / SPEED) + VISUAL_LATENCY
                #'Debug: ' + str(note.idnum) + ". " + str(distance)
                note.move(distance)
                statusc = note.difference
                if -5000 <= statusc <= 5000:
                    combo += 1
                    score += 1000
                    self.hits.append('hit')
                elif statusc == -10000:
                    combo = 0
                    self.hits.append('miss')
                if note.hit == True:
                    self.hits.append('hit')
                    #print('hit')
                if note.miss == True:
                    self.hits.append('miss')
                    #print('miss')

            self.surf.fill((0, 0, 0))
            self.combo_count(combo)
            self.score_count(score)
            self.songprocess(songtime)


            bottomLine = pygame.Rect((width - 1000) / 2, 520, 1000, 8)
            pygame.draw.rect(self.surf, (255, 255, 255), bottomLine)

            Line1 = pygame.Rect(self.medx - 140, self.medy, 5, 600)
            pygame.draw.rect(self.surf, (255, 255, 255), Line1)
            Line2 = pygame.Rect(self.medx - 70, self.medy, 5, 600)
            pygame.draw.rect(self.surf, (255, 255, 255), Line2)
            Line3 = pygame.Rect(self.medx, self.medy, 5, 600)
            pygame.draw.rect(self.surf, (255, 255, 255), Line3)
            Line4 = pygame.Rect(self.medx + 70, self.medy, 5, 600)
            pygame.draw.rect(self.surf, (255, 255, 255), Line4)
            Line5 = pygame.Rect(self.medx + 140, self.medy, 5, 600)
            pygame.draw.rect(self.surf, (255, 255, 255), Line5)

            # self.draw_freq()

            notesD.draw(self.surf)
            notesF.draw(self.surf)
            notesJ.draw(self.surf)
            notesK.draw(self.surf)



            if pygame.mixer.music.get_busy() == False and pause == False:
                running = False

            pygame.display.flip()
            clock.tick(60)

        isfinish = True
        h = []
        m = []
        for hit in self.hits:
            if hit == 'hit':
                h.append(hit)
            if hit =='miss':
                m.append(hit)
        miss = self.hits.count('miss')
        hit = self.hits.count('hit')

        glade = ''
        total = len(self.hits) + len(self.miss)
        persentage = (hit // total) * 100
        if persentage <= 20:
            glade = 'E'
        if 20 < persentage <=40:
            glade = 'D'
        if 40 < persentage <=60:
            glade = 'C'
        if 60 < persentage <=80:
            glade = 'B'
        if 80 < persentage <=90:
            glade = 'A'
        if 90 < persentage <= 100:
            glade = 'S'

        while isfinish:  # mean the game has finished, now to calculate the score
            # direct to result screen
            self.surf.fill((0, 0, 0))
            #
            self.draw_text('END', 65, 120, 120)
            finalscore = self.score

            mx, my = pygame.mouse.get_pos()
            mouse_click = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # isfinish = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_click = True
            self.draw_text(glade, 275, 820, 60)
            self.draw_text('Hits: {}'.format(hit), 24, 100, 300)
            self.draw_text('Miss: {}'.format(miss), 24, 340, 300)
            self.draw_text('Score: {}'.format(finalscore), 44, 100, 400)
            continueBtn = pygame.Rect(50, height - 100, 200, 50)
            pygame.draw.rect(self.surf, (182, 182, 182), continueBtn)
            self.draw_text('Continue', 36, 50, height - 100)
            if continueBtn.collidepoint((mx, my)):
                if mouse_click:
                    isfinish = False
                    self.t_kd.join()
                    self.t_kf.join()
                    self.t_kj.join()
                    self.t_kk.join()

            pygame.display.update()
            clock.tick(60)
