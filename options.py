import sys

import pygame
import pygame_gui

clock = pygame.time.Clock()
font_name = pygame.font.match_font('arial')
width = 1080
height = 720

def draw_text(surf, text, size, x, y):
    font = pygame.font.Font(font_name, size)
    textobj = font.render(text, True, (255, 255, 255))
    text_rect = textobj.get_rect()
    text_rect.topleft = (x, y)
    surf.blit(textobj, text_rect)

#def increaseBgVolume():


def main_settings(screen):
    volume = 0.5
    running = True
    while running:
        screen.fill((0, 0, 0))
        draw_text(screen, 'Settings', 24, width / 2.5, height / 4)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
        pygame.display.update()
        clock.tick(60)

