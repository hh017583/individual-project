import pygame

btn_color = (182, 182, 182)


def draw_text(surf, text, size, color, x, y, font_name = pygame.font.match_font('arial')):
    font = pygame.font.Font(font_name, size)
    textobj = font.render(text, True, color)
    text_rect = textobj.get_rect()
    text_rect.topleft = (x, y)
    surf.blit(textobj, text_rect)


class SongPanel:

    def append(self, song_list):
        self.panel = song_list

    def __init__(self, screen):
        self.panel = []
        self.Panel_colls = []
        self.Button = []
        self.shows_song_array = [0, 1, 2, 3, 4]
        self.selected_song = self.shows_song_array[0]

    '''
     this function to show a list of selectable song button list on game menu
     
    '''

    def show_songlist(self, screen, xpos, ypos, w, h, selected_song, clock):
        x = xpos
        # song = []
        self.selected_song = selected_song

        for song in self.panel:  # song --> object --> songlist
            xpos = x
            if selected_song == song[0]:
                xpos = xpos + 100
                #print("selected song: #" + str(selected_song))
            songname = song[1]
            artist = song[2]
            album = song[3]
            #                panel = pygame.Rect(xpos, ypos, w, h)
            #                pygame.draw.rect(screen, (182, 182, 182), panel)
            s = SongButton()
            s.drawRect(screen, xpos, ypos, w, h)
            self.Button.append(s)

            self.Panel_colls.append(s.getCollider())

            draw_text(screen, songname, 25, (255, 255, 255), xpos, ypos)
            draw_text(screen, artist, 15, (255, 255, 255), xpos, ypos + 50)
            draw_text(screen, album, 15, (255, 255, 255), xpos + 120, ypos + 50)
            ypos += 110


    def update_song_list(self, ctrl):
        if ctrl == 'next':
            self.shows_song_array = [e + 1 for e in self.shows_song_array]
        if ctrl == 'prev':
            self.shows_song_array = [e - 1 for e in self.shows_song_array]

        pass

    def set_selected_song(self, i):
        self.selected_song = i

    # def render(self):
    #    self.font.render


class SongButton():

    def __init__(self):
        self.ax = 0
        self.bx = 0
        self.ay = 0
        self.by = 0

    def drawRect(self, screen, xpos, ypos, w, h):
        self.ax = xpos
        self.ay = ypos
        self.bx = xpos + w
        self.by = ypos + h
        Rect = pygame.Rect(xpos, ypos, w, h)
        pygame.draw.rect(screen, (182, 182, 182), Rect)

    def collidepoint(self, mx, my):
        if self.ax <= mx <= self.bx:
            if self.ay <= my <= self.by:
                return True
        else:
            return False

    def getCollider(self):
        coll = [self.ax, self.ay, self.bx, self.by]
        return coll
