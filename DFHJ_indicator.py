import pygame
import threading


class indicator:
    def __init__(self, surf, medx):
        self.pressed = False
        self.surf = surf
        self.medx = medx
        self.running = True
        self.sleep = False

    def thread1(self):
        self.thd = threading.Thread(target=self.show_ind)
        self.thd.start()

    def thread2(self):
        self.thd2 = threading.Thread(target=self.checkinput)
        self.thd2.start()

    def show_ind(self): # operates in thread1()
        pass

    def destroy(self):
        self.running = False

    def checkinput(self): # operates in thread2()
        pass

    def run(self):
        while self.running:
            self.pressed = False

            self.thread2() # starts input handle thread

            self.thread1() # starts draw indicator object thread()


class D_indicator(indicator):

    def checkinput(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_d]:
            self.pressed = True

    def show_ind(self):
        while self.pressed:
            kd_indicate = pygame.Rect((self.medx - 140) + 10 / 2, 520, 60, 20)
            pygame.draw.rect(self.surf, (255, 0, 0), kd_indicate)
            # pygame.display.flip()
            pygame.display.update(kd_indicate)


class F_indicator(indicator):

    def checkinput(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_f]:
            self.pressed = True

    def show_ind(self):
        while self.pressed:
            kf_indicate = pygame.Rect(self.medx - 70, 520, 60, 20)
            pygame.draw.rect(self.surf, (255, 200, 0), kf_indicate)
            pygame.display.update(kf_indicate)


class K_indicator(indicator):

    def checkinput(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_k]:
            self.pressed = True

    def show_ind(self):
        while self.pressed:
            kk_indicate = pygame.Rect(self.medx + 70, 520, 60, 20)
            pygame.draw.rect(self.surf, (0, 255, 0), kk_indicate)
            pygame.display.update(kk_indicate)


class J_indicator(indicator):
    def checkinput(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_j]:
            self.pressed = True

    def show_ind(self):
        self.checkinput()
        while self.pressed:
            kj_indicate = pygame.Rect(self.medx, 520, 60, 20)
            pygame.draw.rect(self.surf, (255, 255, 0), kj_indicate)
            pygame.display.update(kj_indicate)
