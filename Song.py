import os

from mutagen.mp3 import MP3, HeaderNotFoundError
import mutagen


class Song:
    def __init__(self, _songPath:str):
        self.id = None
        self.album = None
        self.artist = None
        self.title = None
        self.song_Path: str = _songPath
        self.initSongInfo()

    def set_song_id(self, id):
        self.id = id

    def get_song_id(self):
        return self.id

    def tagsList_toString(self, list):

        return_str = ''.join(str(e) for e in list)
        return return_str

    def length_toString(self, audio_length):
        str = ''
        hours = audio_length // 3600
        audio_length %= 3600

        mins = audio_length // 60
        audio_length %= 60
        sec = audio_length
        millsec = (int(sec) - sec) * 1000

        return '{}:{}'.format(int(mins), int(sec))

    def initSongInfo(self):
        tmp = self.song_Path
        if tmp == '':
            print('song path empty')
            return
        print(tmp)
        audio = MP3(tmp)
        song_tags = mutagen.File(tmp, easy=True)

        self.length = audio.info.length
        self.length_inString = self.length_toString(self.length)

        ti = song_tags.get('title', 'None')
        ar = song_tags.get('artist', 'None')
        al = song_tags.get('album', 'None')

        if ti == 'None':
            name, extension = os.path.splitext(self.song_Path)
            self.title = name

        else:
            self.title = self.tagsList_toString(song_tags['title'])
            # self.title = self.tagsList_toString(ti)
        if ar == 'None':
            self.artist = "/"
        else:
            self.artist = self.tagsList_toString(song_tags['artist'])

        if al == 'None':
            self.album = "/"
        else:
            self.album = self.tagsList_toString(song_tags['album'])
