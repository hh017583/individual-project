import pygame


class pane(object):
    #button = pygame.Rect()

    def __init__(self, screen, text, font, size, fcolor, bcolor, x, y, width, height):
        self.font = pygame.font.SysFont(font, size)
        button = pygame.Rect(x, y, width, height)
        self.rect = pygame.draw.rect(screen, bcolor, button)
        self.screen.blit(self.font.render(text, True, fcolor, (width, height)))

        pygame.display.update()
    #def render(self):
    #    self.font.render
