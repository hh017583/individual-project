import pygame
from StartScreen import StartScreen

'''
import pygame_widgets
from pygame_widgets.slider import Slider
from pygame_widgets.textbox import TextBox
'''

'''
pygame.init()
width = 1080
height = 720

StartScreen(width, height, screen)
'''


# from Play import Play
# song_for_testing = './Song/Isolation.mp3'
# map = './Song/Isolation.csv'

# Play(song_for_testing, map, surf=screen)
# pygame.quit

class Main:

    def __init__(self):
        self.width = 1080
        self.height = 720
        self.surf = pygame.display.set_mode((self.width, self.height))
        # print('surf set')
        self.startscreen = StartScreen(self.width, self.height, screen=self.surf)
        self.run()

    def run(self):
        pygame.init()
        print('pygame initialed')
        self.startscreen.run()

if __name__ == '__main__':
    Main()
