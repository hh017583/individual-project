import pygame
import pandas as pd
import csv

clock = pygame.time.Clock()

class rightclickmenu:

    def __init__(self, surf, x, y, selected_song):
        self.x = x
        self.y = y
        self.w = 200
        self.h = 15
        self.surf = surf
        self.bgcolor = (135, 165, 212)
        self.fcolor = (250,255,255)
        self.active = True
        self.selected_song = selected_song
        self.font_name = pygame.font.match_font('arial')

    def draw_text(self, text, size, color, x, y):
        font = pygame.font.Font(self.font_name, size)
        textobj = font.render(text, True, color)
        text_rect = textobj.get_rect()
        text_rect.topleft = (x, y)
        self.surf.blit(textobj, text_rect)

    def drawMenu(self):
        while self.active:
            self.deleteSongbtn = pygame.Rect(self.x, self.y, self.w, self.h)
            pygame.draw.rect(self.surf, self.bgcolor, self.deleteSongbtn)
            self.draw_text('Delete Song', 12, self.fcolor, self.x, self.y)

            mx, my = pygame.mouse.get_pos()
            mouse_click = False

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.active = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_click = True

            if self.deleteSongbtn.collidepoint((mx,my)):
                if mouse_click:
                    self.deleteSongFromLib()
                    self.killMenu()
            if not self.deleteSongbtn.collidepoint((mx,my)):
                if mouse_click:
                    self.killMenu()

            pygame.display.update()
            clock.tick(30)


    def killMenu(self):
        self.active = False

    def deleteSongFromLib(self):


        df = pd.read_csv('./data/song.csv')
        df = df[df.id != self.selected_song]
        df.to_csv('./data/song.csv', index=False)

        # update id
        file = open('./data/song.csv')
        csvreader = csv.reader(file)
        header = next(csvreader)
        rows = []
        for row in csvreader:
            rows.append(row)
        file.close()
        i = 0
        new_rows = []
        for e in rows:
            if e[0] != str(i):
                e[0] = str(i)
            new_rows.append(e)
            i = i + 1
        rows.clear()
        with open('./data/song.csv', 'w') as wfile:
            writer = csv.writer(wfile, delimiter=',', lineterminator='\r')
            writer.writerow(header)
            writer.writerows(new_rows)
            wfile.close()