import csv
import os
import sys
import tkinter.filedialog
import threading
import pygame
import pygame_menu
import numpy
import pandas as pd
from SongPanel import SongPanel

# font_name = pygame.font.match_font('arial')
width = 1080
height = 720

select_song = 0


def draw_text(surf, text, size, color, x, y, font_name=pygame.font.match_font('arial')):
    font = pygame.font.Font(font_name, size)
    textobj = font.render(text, True, color)
    text_rect = textobj.get_rect()
    text_rect.topleft = (x, y)
    surf.blit(textobj, text_rect)



# import music into an AI to recognise the notes of the song
def import_music():
    ft = (('mp3 file', '*.mp3'), ('All files', '*.*'))
    top = tkinter.Tk()
    top.title('Import mp3 music file')
    top.withdraw()
    file = tkinter.filedialog.askopenfilename(parent=top, filetype=ft)

    top.destroy()
    return file

def writeCsv_song_list(filename, filepath, mappath):
    # filepath: /path/to/song/song.mp3
    # mappath:/path/to/song/songmap.csv
    # filename: song

    df = pd.read_csv('./data/song.csv')

    # check the importing song not exist in the database
    condition = df["song_name"] == filename
    index = df.index
    indices = index[condition]
    indices_list = indices.tolist()
    if indices_list:
        print("This music - " + filename + " already exist in index:")
        print(*indices_list)
        pass
    levels = len(df.index)

    with open('./data/song.csv', 'a+') as database:
        from Song import Song
        importSong = Song(filepath)
        info = {'id': levels, 'song_name': filename, 'song_artist': importSong.artist,
                'song_album': importSong.album, 'length': importSong.length_inString, 'offset': '60',
                'audio_path': filepath, 'map_path': mappath}
        data = [info['id'], info['song_name'], info['song_artist'], info['song_album'], info['length'], info['offset'],
                info['audio_path'], info['map_path']]

        writer = csv.writer(database, delimiter=',', lineterminator='\r')
        writer.writerow(data)


def audio_duration(length):
    hours = length // 3600
    length %= 3600
    mins = length // 60
    length %= 60
    seconds = length
    return hours, mins, seconds

class SongLib:
    def __init__(self, width, height, surf):
        self.width = width
        self.height = height
        self.surf = surf
        self.running = False
        self.select_song = -1
        # self.font_name = pygame.font.match_font('arial')
        # self.font = pygame.font.Font(self.font_name, 24)
        self.clock = pygame.time.Clock()
        self.song_selection = type(None)

    def handle_music_import(self):
        # get music file
        f = import_music()
        if f == '':
            return
        # pass file to AI
        from Mapper import Mapper
        mapper = Mapper(self.surf, f)
        # mapper.run()
        if mapper.run() == 'cancel':
            return
        else:
            times, notes = mapper.getRes()

            # classify notes into four class

            for i, n in enumerate(notes):
                note = notes[i]
                if note == 'A' or note == 'A#' or note == 'B':
                    notes[i] = 'd'
                if note == 'C' or note == 'C#' or note == 'D':
                    notes[i] = 'f'
                if note == 'D#' or note == 'E' or note == 'F':
                    notes[i] = 'j'
                if note == 'F#' or note == 'G' or note == 'G#':
                    notes[i] = 'k'

            # write into csv database
            # make file name
            import shutil
            fullpath, extension = os.path.splitext(f)
            import_path, filename = os.path.split(fullpath)
            path = "./Song/"
            # print(filename)
            shutil.copyfile(f, path + filename + extension)

            print("")
            filepath = path + filename + extension.format()
            print("Copy music file to: " + filepath)

            csvf = open(path + filename + '.csv', 'w')

            writer = csv.writer(csvf, delimiter=',', lineterminator='\r')
            # writer.writerow(times)
            # writer.writerow(notes)
            writer.writerows([times, notes])
            csvf.close()
            mappath = path + filename + '.csv'.format()
            print("Wrote map file to : " + mappath)
            # write into song list csv data base

            writeCsv_song_list(filename, filepath, mappath)

    def displayObjectInit(self):
        self.bottom_pane = pygame.Rect(0, 720 - 100, 1080, 100)
        self.btn_importMusic = pygame.Rect((self.width - 200) / 2, 640, 200, 50)
        self.start_btn = pygame.Rect((self.width - 150), (self.height - 150), 500, 500)

    def deleteSongFromLib(self):

        df = pd.read_csv('./data/song.csv')
        df = df[df.songid != self.select_song]  # sort out data frame to without contain the select_song
        df.to_csv('./data/song.csv', index=False)  # write to csv

        # update id attribute in the csv file
        file = open('./data/song.csv')
        csvreader = csv.reader(file)
        header = next(csvreader)
        rows = []
        for row in csvreader:
            rows.append(row)
        file.close()
        i = 0
        new_rows = []
        for e in rows:
            if e[0] != str(i):
                e[0] = str(i)
            new_rows.append(e)
            i = i + 1
        rows.clear()
        with open('./data/song.csv', 'w') as wfile:
            writer = csv.writer(wfile, delimiter=',', lineterminator='\r')
            writer.writerows(new_rows)
            wfile.close()

    def run(self):
        pygame.display.set_caption('Rythem game with AI support')
        self.running = True
        self.displayObjectInit()
        while self.running:

            ### read from csv
            song_df = pd.read_csv("./data/song.csv")
            song_list = []

            # iteration to append song to the song_list
            for each in song_df.itertuples():
                row = [each.id, each.song_name, each.song_artist, each.song_album, each.length, each.offset,
                       each.audio_path,
                       each.map_path]
                song_list.append(row)

            self.surf.fill((0, 0, 0))

            ###
            pygame.draw.rect(self.surf, (182, 182, 182), self.bottom_pane)
            pygame.draw.rect(self.surf, (0, 0, 0), self.btn_importMusic)
            # pygame.draw.rect(screen, (255,255,255), start_btn)
            pygame.draw.circle(self.surf, (162, 162, 162), (self.width - 50, self.height - 50), 110)

            xpos = 50
            ypos = 50
            w = 300
            h = 100

            songPanel = SongPanel(self.surf)  # init songPanel class
            songPanel.append(song_list)

            x = threading.Thread(
                target=songPanel.show_songlist(self.surf, xpos, ypos, w, h, self.select_song, self.clock),
                args=(1,))
            # songPanel.show_songlist(screen, xpos, ypos, w, h, select_song, clock)
            x.start()

            # input
            mx, my = pygame.mouse.get_pos()
            mouse_click = False
            mouse_rightclick = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_click = True
                    if event.button == 3:  # right click
                        mouse_rightclick = True
            x.join()
            if self.btn_importMusic.collidepoint((mx, my)):
                if mouse_click:
                    # f = prompt_music()
                    self.handle_music_import()

            if self.start_btn.collidepoint((mx, my)):
                if mouse_click:
                    print("startBtn caught mouse_click")
                    from Play import Play
                    if self.select_song != -1:
                    # Play(song='testing', length='1.42', artist='testing', map='', surf=sc)
                        print('select_song: ', self.select_song)
                        #print(song_list)
                        gamestart = Play(_songid=self.select_song, song_list=song_list, surf=self.surf)
                        gamestart.play()


            # handle song selection input
            if self.select_song == songPanel.selected_song:
                # global song_selection
                self.song_selection = songPanel.shows_song_array  # define list contain all available song
                i: int = 0
                # handle mouse hovering and clicking on each song panel coll
                for c in songPanel.Panel_colls:  # find the specified array
                    # obtain colliders points for each panel and check if the mouse cursor in the areas
                    ax = c[0]
                    ay = c[1]
                    bx = c[2]
                    by = c[3]
                    if ax <= mx <= bx:
                        if ay <= my <= by:
                            # find the corrisponding song of the location
                            if mouse_click:
                                self.select_song = self.song_selection[i]
                            if mouse_rightclick:
                                self.select_song = self.song_selection[i]
                                from rightclickmenu import rightclickmenu
                                rcBtn = rightclickmenu(self.surf, mx, my, self.select_song)
                                rcBtn.drawMenu()

                    i = i + 1

            # text
            draw_text(self.surf, 'START', 45, (255, 255, 255),
                      width - 120, height - 95)
            draw_text(self.surf, "Import music", 14, (255, 255, 255), width / 2 - 24, height - 55)
            pygame.display.update()
            self.clock.tick(60)
