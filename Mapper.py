import os
import argparse
from pydub import AudioSegment

import audiosegment
import pydub.scipy_effects
import numpy as np
import scipy
import matplotlib.pyplot as plt
import pygame
import pygame_gui
import pygame_widgets
from pygame_widgets.slider import Slider
from pygame_widgets.textbox import TextBox

from Mapper_utils import (
    frequency_spectrum,
    classify_note,
)


class Mapper:
    def __init__(self, _surf, _musicFile):
        self.res_timeframe = []
        self.res_notes = []
        self.surf = _surf
        self.font_name = pygame.font.match_font('arial')
        self.musicFile = _musicFile

    def getRes(self):
        res_notes = self.res_notes
        res_timeframe = self.res_timeframe
        return res_timeframe, res_notes

    def draw_text(self, text, size, color, x, y):
        font = pygame.font.Font(self.font_name, size)
        textobj = font.render(text, True, color)
        text_rect = textobj.get_rect()
        text_rect.topleft = (x, y)
        self.surf.blit(textobj, text_rect)

    # input a song and mapping
    def run(self, plot_fft_indices=[]):

        song = AudioSegment.from_mp3(self.musicFile)

        starts = self.predict_note_starts(song)
        if starts == type(None):
            return 'cancel'

        predicted_notes = self.predict_notes(song, starts, plot_fft_indices)
        print("")
        print("Predicted Notes")
        print(predicted_notes)

    def configuration_panel(self):
        clock = pygame.time.Clock()
        # Size of segments to break song into for volume calculations
        SEGMENT_MS = 5
        # Minimum volume necessary to be considered a note
        VOLUME_THRESHOLD = -35
        # The increase from one sample to the next required to be considered a note
        EDGE_THRESHOLD = 5
        # Throw out any additional notes found in this window
        MIN_MS_BETWEEN = 100
        color = (92, 115, 45)
        menu_width = 1080 / 1.5
        menu_height = 720 / 1.5

        width = 1080
        height = 720

        slider_1 = Slider(self.surf, 300, 300, 140, 10, min=1, max=100, step=1)
        slider_1.setValue(SEGMENT_MS)
        o_slider_1 = TextBox(self.surf, 300 + 250, 300, 150, 30, fontSize=20)

        vol_thre_slider = Slider(self.surf, 300, 350, 140, 10, min=-50 + 100, max=0 + 100)
        vol_thre_slider.setValue(100 + VOLUME_THRESHOLD)
        vol_thre_textbox = TextBox(self.surf, 550, 350, 150, 30, fontSize=20)

        edge_thre_slider = Slider(self.surf, 300, 400, 140, 10, min=1, max=10, step=1)
        edge_thre_slider.setValue(EDGE_THRESHOLD)
        edge_thre_textbox = TextBox(self.surf, 550, 400, 150, 30, fontSize=20)

        min_ms_slider = Slider(self.surf, 300, 450, 140, 10, min=50, max=200, step=1)
        min_ms_slider.setValue(MIN_MS_BETWEEN)
        min_ms_textbox = TextBox(self.surf, 550, 450, 150, 30, fontSize=20)

        mouse_click = False
        temp_SEGMENT_MS = SEGMENT_MS
        temp_VOLUME_THRESHOLD = VOLUME_THRESHOLD
        temp_EDGE_THRESHOLD = EDGE_THRESHOLD
        temp_MIN_MS_BETWEEN = MIN_MS_BETWEEN
        run = True
        while run:
            mx, my = pygame.mouse.get_pos()

            panel_bg = pygame.Rect((width / 2 - menu_width / 2),
                                   (height / 2 - menu_height / 2),
                                   menu_width, menu_height)
            pygame.draw.rect(self.surf, color, panel_bg, border_radius=5)
            continue_btn = pygame.Rect(menu_width / 2 - 30, 720 / 1.34, 150, 55)
            cancel_btn = pygame.Rect(menu_width / 2 + 200, 720 / 1.34, 150, 55)
            # slider

            temp_SEGMENT_MS = slider_1.getValue()
            o_slider_1.setText('SEGMENT_MS:' + str(temp_SEGMENT_MS))

            temp_VOLUME_THRESHOLD = vol_thre_slider.getValue() - 100
            vol_thre_textbox.setText('Volume Threshold:' + str(temp_VOLUME_THRESHOLD))

            temp_EDGE_THRESHOLD = edge_thre_slider.getValue()
            edge_thre_textbox.setText('Edge Threshold:' + str(temp_EDGE_THRESHOLD))

            temp_MIN_MS_BETWEEN = min_ms_slider.getValue()
            min_ms_textbox.setText('Min Ms Between:' + str(temp_MIN_MS_BETWEEN))

            # show buttons

            pygame.draw.rect(self.surf, (182, 182, 182), continue_btn)
            self.draw_text('Continue', 23, (250, 255, 255), menu_width / 2 - 30, 720 / 1.34)
            pygame.draw.rect(self.surf, (182, 182, 182), cancel_btn)
            self.draw_text('Cancel', 23, (250, 255, 255), menu_width / 2 + 200, 720 / 1.34)

            if continue_btn.collidepoint((mx, my)):
                pygame.draw.rect(self.surf, (120, 120, 120), continue_btn)
                self.draw_text('Continue', 23, (250, 255, 255), menu_width / 2 - 30, 720 / 1.34)
                if mouse_click:
                    # print('continue button clicked')
                    SEGMENT_MS = temp_SEGMENT_MS
                    VOLUME_THRESHOLD = temp_VOLUME_THRESHOLD
                    EDGE_THRESHOLD = temp_EDGE_THRESHOLD
                    MIN_MS_BETWEEN = temp_MIN_MS_BETWEEN
                    run = False
            if cancel_btn.collidepoint((mx, my)):
                pygame.draw.rect(self.surf, (120, 120, 120), cancel_btn)
                self.draw_text('Cancel', 23, (250, 255, 255), menu_width / 2 + 200, 720 / 1.34)
                if mouse_click:
                    # print('cancel button clicked')
                    SEGMENT_MS = type(None)
                    VOLUME_THRESHOLD = type(None)
                    EDGE_THRESHOLD = type(None)
                    MIN_MS_BETWEEN = type(None)
                    run = False

            # input handle

            events = pygame.event.get()
            mouse_click = False
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        print('esc pressed')
                        SEGMENT_MS = type(None)
                        VOLUME_THRESHOLD = type(None)
                        EDGE_THRESHOLD = type(None)
                        MIN_MS_BETWEEN = type(None)
                        run = False
                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        mouse_click = True
                        # print(mouse_click)

            pygame_widgets.update(events)
            pygame.display.update()
            clock.tick(60)

        slider_1.disable()
        o_slider_1.disable()
        return SEGMENT_MS, VOLUME_THRESHOLD, EDGE_THRESHOLD, MIN_MS_BETWEEN

    def predict_note_starts(self, song):

        SEGMENT_MS, VOLUME_THRESHOLD, EDGE_THRESHOLD, MIN_MS_BETWEEN = self.configuration_panel()
        print('')
        print('SEGMENT_MS:', SEGMENT_MS)
        print('VOLUME_THRESHOLD:', VOLUME_THRESHOLD)
        print('EDGE_THRESHOLD:', EDGE_THRESHOLD)
        print('MIN_MS_BETWEEN:', MIN_MS_BETWEEN)

        predicted_starts = []

        if SEGMENT_MS == type(None):
            predicted_starts = type(None)
            return predicted_starts

        # Filter out lower frequencies to reduce noise
        song = song.high_pass_filter(80, order=4)
        # dBFS is decibels relative to the maximum possible loudness
        volume = [segment.dBFS for segment in song[::SEGMENT_MS]]

        for i in range(1, len(volume)):
            if volume[i] > VOLUME_THRESHOLD and volume[i] - volume[i - 1] > EDGE_THRESHOLD:
                ms = i * SEGMENT_MS
                # Ignore any too close together
                if len(predicted_starts) == 0 or ms - predicted_starts[-1] >= MIN_MS_BETWEEN:
                    predicted_starts.append(ms)

        return predicted_starts

    def predict_notes(self, song, starts, plot_fft_indices):
        predicted_notes = []
        for i, start in enumerate(starts):
            sample_from = start + 50
            sample_to = start + 550
            if i < len(starts) - 1:
                sample_to = min(starts[i + 1], sample_to)
            segment = song[sample_from:sample_to]
            freqs, freq_magnitudes = frequency_spectrum(segment)

            predicted = classify_note(freqs, freq_magnitudes)
            predicted_notes.append(predicted or "U")

            # Print general info
            print("")
            print("Note: {}".format(i))

            print("Predicted: {}".format(predicted))
            print("Predicted start: {}".format(start))
            length = sample_to - sample_from
            print("Sampled from {} to {} ({} ms)".format(sample_from, sample_to, length))
            print("Frequency sample period: {}hz".format(freqs[1]))

            # append timeframe and note prediction into global list

            self.res_timeframe.append(start)
            self.res_notes.append('{}'.format(predicted))

            # Print peak info
            peak_indicies, props = scipy.signal.find_peaks(freq_magnitudes, height=0.015)
            print("Peaks of more than 1.5 percent of total frequency contribution:")
            for j, peak in enumerate(peak_indicies):
                freq = freqs[peak]
                magnitude = props["peak_heights"][j]
                print("{:.1f}hz with magnitude {:.3f}".format(freq, magnitude))

        return predicted_notes
